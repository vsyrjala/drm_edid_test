CPPFLAGS:=-Wall -Wextra -Wno-missing-field-initializers -Wno-unused-parameter -Wno-sign-compare -Iinclude -include include/compat/compat.h
CFLAGS:=-std=gnu99 -O0 -g3

drm_SOURCES:=drm_edid.c drm_modes.c drm_crtc.c list_sort.c compat.c
test_SOURCES:=test.c

TARGETS:=test

.PHONY: all clean

all: $(TARGETS)

test: $(test_SOURCES:.c=.o) libdrm_edid.a
	$(CC) $(LDFLAGS) -o $@ $^

%.d: %.c Makefile
	$(CC) $(CPPFLAGS) $(CFLAGS) -MM -MF $@ $<

%.o: %.c Makefile
	$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

libdrm_edid.a: $(drm_SOURCES:.c=.o)
	$(AR) cru $@ $^

clean::
	-rm -rf $(TARGETS) *.o *.d *.a

-include $(drm_SOURCES:.c=.d)
-include $(test_SOURCES:.c=.d)
