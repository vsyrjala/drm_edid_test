#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include <drm/drmP.h>
#include <drm/drm_crtc.h>
#include <drm/drm_edid.h>

static struct drm_device dev;
static struct drm_connector connector;

static const struct drm_connector_funcs funcs =
{
};

static const char *color_formats_str(u32 color_formats)
{
	static char str[32];

	if (color_formats & DRM_COLOR_FORMAT_RGB444)
		strcat(str, "RGB444,");
	if (color_formats & DRM_COLOR_FORMAT_YCRCB444)
		strcat(str, "YCRCB444,");
	if (color_formats & DRM_COLOR_FORMAT_YCRCB422)
		strcat(str, "YCRCB422,");

	return str;
}

#define SUB_PIXEL_ORDER(x) case SubPixel ##x: return #x

static const char *subpixel_order_str(enum subpixel_order subpixel_order)
{
	switch (subpixel_order) {
	SUB_PIXEL_ORDER(Unknown);
	SUB_PIXEL_ORDER(HorizontalRGB);
	SUB_PIXEL_ORDER(HorizontalBGR);
	SUB_PIXEL_ORDER(VerticalRGB);
	SUB_PIXEL_ORDER(VerticalBGR);
	SUB_PIXEL_ORDER(None);
	default: return "";
	};
}

#define XSTR(s) STR(s)
#define STR(s) #s

int main(int argc, char *argv[])
{
	int r;
	struct drm_display_mode *mode;
	struct edid *edid;
	struct stat stat;
	int fd;

	if (argc != 2){
		fprintf(stderr, "Usage: %s <EDID file>\n",
			argv[0]);
		return 1;
	}

	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "%s: Unable to open '%s': %d:%s\n",
			argv[0], argv[1], errno, strerror(errno));
		return 2;
	}

	r = fstat(fd, &stat);
	if (r) {
		fprintf(stderr, "%s: Unable to stat '%s': %d:%s\n",
			argv[0], argv[1], errno, strerror(errno));
		close(fd);
		return 3;
	}

	if (stat.st_size == 0 || stat.st_size % 128 != 0) {
		fprintf(stderr, "%s: File '%s' has bad size: %zu\n",
			argv[0], argv[1], stat.st_size);
		close(fd);
		return 4;
	}

	edid = malloc(stat.st_size);
	if (edid == NULL) {
		fprintf(stderr, "%s: Unable to allocate EDID data of size %zu\n",
			argv[0], stat.st_size);
		close(fd);
		return 5;
	}

	r = read(fd, edid, stat.st_size);
	if (r != (int) stat.st_size) {
		fprintf(stderr, "%s: Unable to read from '%s': %d:%s\n",
			argv[0], argv[1], errno, strerror(errno));
		free(edid);
		close(fd);
		return 6;
	}

	close(fd);

	drm_mode_config_init(&dev);

	drm_connector_init(&dev, &connector, &funcs, DRM_MODE_CONNECTOR_VGA);

	r = drm_add_edid_modes(&connector, edid);
	if (r < 0) {
		fprintf(stderr, "%s: Unable to parse modes from EDID: %d:%s\n",
			argv[0], -r, strerror(-r));
		free(edid);
		return 7;
	}

	drm_mode_connector_list_update(&connector);

	printk("display_info:\n"
	       " name = %s\n"
	       " width_mm = %u\n"
	       " height_mm = %u\n"
	       " min_vfreq = %u\n"
	       " max_vfreq = %u\n"
	       " min_hfreq = %u\n"
	       " max_hfreq = %u\n"
	       " pixel_clock = %u\n"
	       " bpc = %u\n"
	       " subpixel_order = %s\n"
	       " color_formats = %s\n"
	       " cea_rev = 0x%x\n",
	       connector.display_info.name,
	       connector.display_info.width_mm,
	       connector.display_info.height_mm,
	       connector.display_info.min_vfreq,
	       connector.display_info.max_vfreq,
	       connector.display_info.min_hfreq,
	       connector.display_info.max_hfreq,
	       connector.display_info.pixel_clock,
	       connector.display_info.bpc,
	       subpixel_order_str(connector.display_info.subpixel_order),
	       color_formats_str(connector.display_info.color_formats),
	       connector.display_info.cea_rev);

	printk("hdmi = %s\n"
	       "audio = %s\n",
	       drm_detect_hdmi_monitor(edid) ? "yes" : "no",
	       drm_detect_monitor_audio(edid) ? "yes" : "no");

	printk("user_modes:\n");
	list_for_each_entry(mode, &connector.user_modes, head)
		drm_mode_debug_printmodeline(mode);
	printk("probed_modes:\n");
	list_for_each_entry(mode, &connector.probed_modes, head)
		drm_mode_debug_printmodeline(mode);
	printk("modes:\n");
	list_for_each_entry(mode, &connector.modes, head)
		drm_mode_debug_printmodeline(mode);

	free(edid);

	return 0;
}
