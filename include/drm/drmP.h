#ifndef DRMP_H
#define DRMP_H

#include <drm/drm_crtc.h>
#include <drm/drm.h>
#include <linux/list.h>

struct device;

struct drm_driver {
	int (*dumb_create)(struct drm_file *file_priv,
			   struct drm_device *dev,
			   struct drm_mode_create_dumb *args);
	int (*dumb_map_offset)(struct drm_file *file_priv,
			       struct drm_device *dev, uint32_t handle,
			       uint64_t *offset);
        int (*dumb_destroy)(struct drm_file *file_priv,
			    struct drm_device *dev,
			    uint32_t handle);
};

struct drm_device
{
	struct device *dev;
	struct drm_driver *driver;
	struct drm_mode_config mode_config;
};

struct drm_cmdline_mode {
	bool specified;
	bool refresh_specified;
	bool bpp_specified;
	int xres, yres;
	int bpp;
	int refresh;
	bool rb;
	bool interlace;
	bool cvt;
	bool margins;
	enum drm_connector_force force;
};

extern int drm_sysfs_connector_add(struct drm_connector *connector);
extern void drm_sysfs_connector_remove(struct drm_connector *connector);

#define DRIVER_MODESET     0x2000

static __inline__ int drm_core_check_feature(struct drm_device *dev,
					     int feature)
{
	return feature == DRIVER_MODESET;
}

#define DRM_MINOR_CONTROL 2

struct drm_minor {
	int type;
	struct drm_mode_group mode_group;
        struct drm_device *dev;
};

struct drm_master {
	struct drm_minor *minor;
};

struct drm_file {
	struct list_head fbs;
	struct drm_master *master;
	struct drm_minor *minor;
	int event_space;
};

struct drm_pending_event {
	struct drm_event *event;
	struct list_head link;
	struct drm_file *file_priv;
	pid_t pid;
	/* pid of requester, no guarantee it's valid by the time
	   we deliver the event, for tracing only */
	void (*destroy)(struct drm_pending_event *event);
};

struct drm_pending_vblank_event {
	struct drm_pending_event base;
	int pipe;
	struct drm_event_vblank event;
};



#endif
