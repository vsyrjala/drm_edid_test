#ifndef DRM_H
#define DRM_H

struct drm_clip_rect {
	unsigned short x1;
	unsigned short y1;
	unsigned short x2;
	unsigned short y2;
};

#define DRM_EVENT_FLIP_COMPLETE 0x02

struct drm_event {
	__u32 type;
	__u32 length;
};

struct drm_event_vblank {
	struct drm_event base;
	__u64 user_data;
	__u32 tv_sec;
	__u32 tv_usec;
	__u32 sequence;
	__u32 reserved;
};

#endif
