#ifndef COMPAT_TYPES_H
#define COMPAT_TYPES_H

#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef uint8_t __u8;
typedef uint16_t __u16;
typedef uint32_t __u32;
typedef uint64_t __u64;

typedef int8_t __s8;
typedef int16_t __s16;
typedef int32_t __s32;
typedef int64_t __s64;

typedef uint16_t __le16;
typedef uint32_t __le32;
typedef uint64_t __le64;

typedef uint16_t __be16;
typedef uint32_t __be32;
typedef uint64_t __be64;

#define EXPORT_SYMBOL(x);

#define __read_mostly

#define MODULE_PARM_DESC(x...);
#define module_param_named(x...);

struct i2c_msg;
struct seq_file;

struct mutex {
	int x;
};

struct idr {
	int x;
};

struct kref {
	int x;
};

struct device {
	int x;
};

struct delayed_work {
	int x;
};

struct i2c_adapter {
	const char *name;
};

typedef size_t resource_size_t;

#define container_of(ptr, type, member) ({		     \
	const typeof(((type *)NULL)->member) *__ptr = (ptr); \
	(type*)((char*)__ptr - offsetof(type, member));	     \
})

#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]))

#define KERN_ERR ""
#define KERN_WARNING ""
#define KERN_DEBUG ""

#define printk printf
#define printk_once printf
#define DRM_DEBUG printf
#define DRM_DEBUG_KMS printf
#define DRM_ERROR printf

void print_hex_dump(const char *level,
		    const char *prefix_str,
		    int prefix_type,
		    int rowsize,
		    int groupsize,
		    const void *buf,
		    size_t len,
		    bool ascii);

#define DUMP_PREFIX_NONE 0

int i2c_transfer(struct i2c_adapter *adap,
		 struct i2c_msg *msgs, int num);

int memchr_inv(const void *buf, int c, size_t len);

#define DRM_UT_KMS 1

extern int drm_debug;

#define GFP_KERNEL 0

typedef int gfp_t;

void *kmalloc(size_t len, gfp_t flags);
void *krealloc(void *old, size_t len, gfp_t flags);
void *kzalloc(size_t len, gfp_t flags);
void kfree(void *p);

#define dev_warn(dev, fmt...) printf(fmt)

__le16 cpu_to_le16(u16 x);
u16 le16_to_cpu(__le16 x);

#define uninitialized_var(x) x

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

#define simple_strtol strtol

#define LIST_POISON1 ((void *)0xdeadbeef)
#define LIST_POISON2 ((void *)0xf00baff0)

struct list_head {
	struct list_head *next, *prev;
};

struct hlist_head {
	struct hlist_node *first;
};

struct hlist_node {
	struct hlist_node *next, **pprev;
};

#define KHZ2PICOS(a) (1000000000UL/(a))

void mutex_init(struct mutex *lock);
void mutex_lock(struct mutex *lock);
void mutex_unlock(struct mutex *lock);
int mutex_is_locked(struct mutex *lock);

void idr_init(struct idr *idp);
int idr_pre_get(struct idr *idp, gfp_t gfp_mask);
int idr_get_new_above(struct idr *idp, void *ptr,
		      int starting_id, int *id);
int idr_remove(struct idr *idp, int id);
void *idr_find(struct idr *idp, int id);
void idr_remove_all(struct idr *idp);
void idr_destroy(struct idr *idp);

void kref_init(struct kref *kref);
void kref_get(struct kref *kref);
void kref_put(struct kref *kref,
	      void (*release)(struct kref *kref));

#define unlikely(x) (x)

#define WARN(COND, ...) do { if (COND) printf(__VA_ARGS__); } while (0)
#define WARN_ON(COND) do { if (COND) printf("WARN\n"); } while (0)

#define __user
#define __must_check

#define swap(a, b) \
do { \
	typeof(a) __tmp = (a); \
	(a) = (b); \
	(b) = __tmp; \
} while (0)

#define get_user(x, ptr) (0)
#define put_user(x, ptr) (0)

#define MAX_ERRNO 4095
#define IS_ERR_VALUE(x) unlikely((x) >= (unsigned long)-MAX_ERRNO)

static inline void * __must_check ERR_PTR(long error)
{
	return (void *) error;
}

static inline long __must_check PTR_ERR(const void *ptr)
{
	return (long) ptr;
}

static inline long __must_check IS_ERR(const void *ptr)
{
	return IS_ERR_VALUE((unsigned long)ptr);
}

static inline long copy_from_user(void *to,
				  const void __user *from,
				  unsigned long n)
{
	return 0;
}

static inline long copy_to_user(void __user *to,
				const void *from,
				unsigned long n)
{
	return 0;
}

#define spin_lock_irqsave(lock, flags) do {} while (0)
#define spin_unlock_irqrestore(lock, flags) do {} while (0)

#endif
