#include "compat/compat.h"

int drm_debug = 0;

void mutex_init(struct mutex *lock)
{
}

void mutex_lock(struct mutex *lock)
{
}

void mutex_unlock(struct mutex *lock)
{
}

int mutex_is_locked(struct mutex *lock)
{
	return 0;
}

void idr_init(struct idr *idp)
{
}

int idr_pre_get(struct idr *idp, gfp_t gfp_mask)
{
	return 1;
}

int idr_get_new_above(struct idr *idp, void *ptr,
		      int starting_id, int *id)
{
	static int next;

	if (next < starting_id)
		next = starting_id;

	if (next == INT_MAX)
		return -ENOMEM;

	*id = next++;

	return 0;
}

int idr_remove(struct idr *idp, int id)
{
	return 0;
}

void *idr_find(struct idr *idp, int id)
{
	return NULL;
}

void idr_remove_all(struct idr *idp)
{
}

void idr_destroy(struct idr *idp)
{
}

void print_hex_dump(const char *level,
		    const char *prefix_str,
		    int prefix_type,
		    int rowsize,
		    int groupsize,
		    const void *buf,
		    size_t len,
		    bool ascii)
{
}

int i2c_transfer(struct i2c_adapter *adap,
		 struct i2c_msg *msgs, int num)
{
	return 0;
}

int memchr_inv(const void *buf, int c, size_t len)
{
	const uint8_t *ptr = buf;

	while (len--) {
		if (*ptr++ != c)
			return 1;
	}

	return 0;
}

void *kmalloc(size_t len, gfp_t flags)
{
	return malloc(len);
}

void *krealloc(void *old, size_t len, gfp_t flags)
{
	return realloc(old, len);
}

void *kzalloc(size_t len, gfp_t flags)
{
	return calloc(1, len);
}

void kfree(void *p)
{
	free(p);
}

__le16 cpu_to_le16(u16 x)
{
	return x;
}

u16 le16_to_cpu(__le16 x)
{
	return x;
}

void kref_init(struct kref *kref)
{
}

void kref_get(struct kref *kref)
{
}

void kref_put(struct kref *kref,
	      void (*release)(struct kref *kref))
{
}

struct drm_connector;

void drm_sysfs_connector_add(struct drm_connector *connector)
{
}

void drm_sysfs_connector_remove(struct drm_connector *connector)
{
}
